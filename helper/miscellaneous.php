<?php

/**
 * hash helper function
 * @param $string
 * @return string
 */
function bcrypt($string)
{
    return hash('sha256',$string);
}

