<?php

//define controllers here
//here using global $container that define in index.php

//define controller
$container['HomeController'] = function ($container) {
    return   new \App\controller\HomeController($container);
};


//AuthController investor
$container['AuthController'] = function ($container) {
    return   new \App\controller\auth\AuthController($container);
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Admin's Controllers~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
//Admin - AuthController
$container['AdminAuthController'] = function ($container) {
    return   new \App\controller\admin\AdminAuthController($container);
};

//Admin Product Controller
$container['AdminProductController'] = function ($container) {
    return   new \App\controller\admin\AdminProductController($container);
};

//Admin Product Item Controller
$container['AdminProductItemController'] = function ($container) {
    return   new \App\controller\admin\AdminProductItemController($container);
};

//Admin investor Controller
$container['AdminInvestorController'] = function ($container) {
    return   new \App\controller\admin\AdminInvestorController($container);
};








