<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/7/17
 * Time: 4:47 PM
 */

namespace App\service;


class PaymentCalculator
{

    public $_amount;
    public $_reinvest;
    public $_startRange;
    public $_durationRange;
    public $_percent=0.03;


    public $_cf;

    public function __construct($amount  , $reinvest , $startRange , $durationRange)
    {
        $this->_amount = $amount;
        $this->_reinvest = $reinvest;
        $this->_startRange = $startRange;
        $this->_durationRange = $durationRange;
    }


    public function __invoke()
    {

        return [
          'CF'=>$this->cf(),
          'ACF'=>$this->acf()
        ];
    }

    /**
     * calculate annual
     * var annuityFormula =  (investPrice -
     ((1 - Math.pow((1+interestRate), (-rangeStart+1)))/interestRate)*reinvest) *(Math.pow((1+interestRate) , (rangeStart-1)))
             / ((1-Math.pow(((1+interestRate)) , (-rangeDuration)))/interestRate)
     * @return float|int
     */
    public function annual()
    {
        $p1 =  1 - pow( (1+$this->_percent), (1-$this->_startRange) ) ;
        $p2 = pow( (1+$this->_percent) , ($this->_startRange-1) );
        $p3 = 1- pow(((1+$this->_percent)) , (-$this->_durationRange));

        return  ($this->_amount - ( $p1 /$this->_percent) * $this->_reinvest) *( $p2 ) / ( $p3 /$this->_percent);
    }


    /**
     * calculate cf from excel file (-_-)
     *
     * @return array
     */
    public function cf()
    {
        $m = $this->_startRange + $this->_durationRange;//19
        $cf=[];
        $total = 0;
        $annual = $this->annual();
        for ($i=0 ; $i<$m ; $i++)
        {
            if($i==0)
            {
                $cf[] = $this->_amount;
                continue;
            }
            if($i  <   $this->_startRange)
            {
                $cf[] = $this->_reinvest;

            }else
            {
                $cf[] = $annual;
            }
        }
        $this->_cf = $cf;
        return $cf;
    }

    /**
     *
     */
    public function acf()
    {
        $acf=[];
        $cf =$this->_cf;
        $amount  =$this->_amount;
        for($i = 1 ; $i <= $this->_startRange+$this->_durationRange ; $i++)
        {


            if($i == 1)
            {
               $acf[] =$amount;
               continue;
            }

            if(count($cf) >= $i)
            {
                if(   $cf[ $i - 1]    == $amount ) continue;
                $amount += -$cf[$i - 1];
                $acf[] = round($amount);
            }
            else
            {
               $acf[] =$acf[count($acf)-1];
            }


        }
        return $acf;
    }


}