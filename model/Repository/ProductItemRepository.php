<?php
namespace Model\Repository;


use Model\Product;
use Model\ProductItem;

class ProductItemRepository
{

    public $_model ;


    public function __construct()
    {
        $this->_model = new ProductItem();
    }

    /**
     * store new item
     * @param $id
     * @param array $data
     * @return array
     */
    public function storeItem($id , array  $data)
    {
        if( $product = Product::find($id))
        {
            //todo :: set attribute

            if($product->productItem()->create($data))
            {
                return ['status'=>201,'message'=>'ITEM HAS BEEN created'];
            }
            return ['status'=>203,'message'=>'incorrect data.'];
        }
        return ['status'=>'203','message'=>'incorrect product.'];

    }



    /**
     * delete item's product by id
     * @param $id
     * @return bool
     */
    public function  deleteItem($id)
    {
        return !!$this->_model->where('id',$id)->delete();
    }





}