<?php
namespace Model\Repository;


use Model\Investor;

class InvestorRepository
{

    public $_model ;


    public function __construct()
    {
        $this->_model = new Investor();
    }

    /**
     * return all user
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllUsers()
    {
        return $this->_model->all();
    }

    /**
     *
     * @param $email
     */
    public function getUserByEmail($email)
    {
        return $this->_model->where('email',$email)->first();
    }

    /**
     * @param $api_key
     */
    public function getUserByApiKey($api_key)
    {
        return $this->_model->where('api_key',$api_key)->first();
    }

    /**
     * show user's information
     * @param $id
     *
     * @return array
     */
    public function showInvestor($id)
    {
        if($user = $this->_model->find($id))
        {
            $user->load('investorBankInformation');
            return ['status'=>200,'investor'=>$user];
        }
        return ['status'=>203,'message'=>'اطلاعات ارسالی نادرست.'];
    }


    /**
     * change password
     * @param $id
     * @param $password
     * @param $passwordConfirmation
     *
     * @return array
     */
    public function changePasswordByAdmin($id,$password,$passwordConfirmation)
    {
        if($investor= $this->_model->find($id))
        {
            if($password ===$passwordConfirmation)
            {
                $investor->password = bcrypt($password);
                if ($investor->save())
                {
                    return ['status'=>202,'message'=>'کلمه عبور با موفقیت تغیر یافت.','investor'=>$investor];
                }
                return ['status'=>203,'message'=>'کلمه عبور و تکرار آن ناهمخوان هستند.'];
            }
        }
        return ['status'=>203,'message'=>'داده نامعتبر'];
    }


}