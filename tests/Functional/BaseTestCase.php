<?php

namespace Tests\Functional;

use Model\Admin;
use Model\Investor;
use SebastianBergmann\GlobalState\RuntimeException;
use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Environment;

/**
 * This is an example class that shows how you could set up a method that
 * runs the application. Note that it doesn't cover all use-cases and is
 * tuned to the specifics of this skeleton app, so if your needs are
 * different, you'll need to change it.
 */
class BaseTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * Use middleware when running application?
     *
     * @var bool
     */
    protected $withMiddleware = false;

    /**
     * Process the application given a request method and URI
     *
     * @param string $requestMethod the request method (e.g. GET, POST, etc.)
     * @param string $requestUri the request URI
     * @param array|object|null $requestData the request data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function runApp($requestMethod, $requestUri, $requestData = null)
    {
        // Create a mock environment for testing with
        $environment = Environment::mock(
            [
                'REQUEST_METHOD' => $requestMethod,
                'REQUEST_URI' => $requestUri,
            ]
        );

        // Set up a request object based on the environment
        $request = Request::createFromEnvironment($environment);

        // Add request data, if it exists
        if (isset($requestData)) {
            $request = $request->withParsedBody($requestData);
        }

        // Set up a response object
        $response = new Response();

        // Use the application settings
        require __DIR__ . '/../../bootstrap/app.php';


        // Process the application
         $r = $app->process($request, $response);
        // check all application return json value
//        $this->assertContains('application/json;charset=utf-8',$r->getHeader('Content-Type')[0]);
        return $r;
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Request Type POST|GET|PUT|DELETE

    /**
     * @param $requestUri
     * @param null $requestData
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function post($requestUri , $requestData = null)
    {
        return $this->runApp('POST',$requestUri,$requestData);
    }

    /**
     * @param $requestUri
     * @param null $requestData
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function get($requestUri , $requestData = null)
    {
        return $this->runApp('GET',$requestUri,$requestData);
    }

    /**
     * @param $requestUri
     * @param null $requestData
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function put($requestUri  , $requestData = null)
    {
        return $this->runApp('PUT',$requestUri,$requestData);
    }

    /**
     * @param $requestUri
     * @param null $requestData
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete($requestUri , $requestData = null)
    {
        return $this->runApp('DELETE',$requestUri,$requestData);
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ LOGIN todo:: use auth class

    /**
     * login admin
     * @return mixed
     */
    public function beAdmin()
    {
        $admin = Admin::inRandomOrder()->first();

        $admin->api_key = bcrypt($admin->email);
        $admin->save();
        return $admin;
    }

    /**
     * login investor
     * @return mixed
     */
    public function beInvestor()
    {
        $admin = Investor::inRandomOrder()->first();

        $admin->api_key = bcrypt($admin->email);
        $admin->save();
        return $admin;
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



}
