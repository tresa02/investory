<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/2/17
 * Time: 12:05 PM
 */

namespace App\controller\admin;


use App\controller\baseController;
use App\service\Auth\AdminAuth;
use Middleware\AuthUserMiddleware;
use Slim\Http\Request;
use Slim\Http\Response;

class AdminAuthController extends baseController
{

    /**
     * login action
     * @param Request $request
     * @param Response $response
     * @return string
     */
    public function login(Request $request,Response $response)
    {
        $username  =$request->getParam('email' ,0);
        $password = $request->getParam('password',0);
        if($key = AdminAuth::login($username,$password))
        {
            return $response->withStatus(202)->withJson(['authorization'=>$key]);
        }

        return $response->withStatus(203)->withJson(['message'=>"username or email incorrect."]);
    }

    /**
     * logout
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function logout(Request $request , Response $response)
    {
        //todo ::change this comment in production
//        $api_key = $request->getHeader('Authorization')?:['0'];
        $api_key = $request->getParam('Authorization',[0]);

        if( AdminAuth::logout($api_key) )
        {
            return $response->withStatus(200)->withJson(['message'=>'شما با موفقیت از سیستم خارج شدید.']);
        }
        return $response->withStatus(203)->withJson(['message'=>'مشکلی در خروج از سیستم پیش آمده است.']);
    }

    




}
