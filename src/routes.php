<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/[{name}]', function (Request $request, Response $response, array $args)
{
    $investor = Model\Investor::whereHas('investorBankInformation')->with(['investorBankInformation'=>function($query){
        $query->first();
    }])->first();

        return $response->withJson($investor->investorBankInformation[0]->id);

});

$app->post('/[{name}]', function (Request $request, Response $response, array $args)
{

})->add(\Middleware\Validator\Admin\ProductValidator::class);



$app->post('/admin/login', 'AdminAuthController:login');
$app->post('/admin/logout', 'AdminAuthController:logout');

//~~~~~~~~~~~~~~~~~ADMIN~~~~~~~~~~~~~~~~~~~~~~~~ADMIN~~~~~~~~~~~~~ADMIN

$app->group('/admin',function () {

    //products
    $this->get('/products', 'AdminProductController:index');//get all products
    $this->get('/products/{id}', 'AdminProductController:show');//store new product
    $this->post('/products/store', 'AdminProductController:store');//store new product
    $this->put('/products/{id}/update', 'AdminProductController:update');//store new product
    $this->delete('/products/{id}', 'AdminProductController:destroy');//delete the product

    //product-item
    $this->post('products/{id}/add-item','AdminProductItemController:store');//add item to product
    $this->delete('products/{id}/item/{item-id}','AdminProductItemController:destroy');//delete item product

    //users
    $this->get('/investor','AdminInvestorController:index');//show all users
    $this->get('/investor/{id}','AdminInvestorController:show');// show user info
    $this->post('/investor/{id}/add-bank','AdminInvestorController:storeBankInformation');//add new bank to investor
    $this->delete('/investor/{id}/bank/{bank_id}','AdminInvestorController:destroyBankInformation');//delete bank investor
    $this->put('/investor/{id}/bank/{bank_id}/update','AdminInvestorController:updateBankInformation');//update bank information investor
    $this->put('/investor/{id}/change-password','AdminInvestorController:changePassword');//update bank information investor

});



