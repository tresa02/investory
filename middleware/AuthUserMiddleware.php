<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/4/17
 * Time: 10:00 AM
 */

namespace Middleware;



use App\service\Auth\Auth;

class AuthUserMiddleware
{



    public function __invoke($request, $response, $next)
    {

        $header = $request->getHeader('Authorization');
        if(!$header || !Auth::check($header))
        {
            return $response->withStatus(401)->withJson(
                [
                    'message'=>"you not logged in.",
                ]);

        }

        $response = $next($request, $response);

        return $response;

    }

}