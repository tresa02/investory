<?php namespace App\controller\admin;


use App\controller\baseController;
use Model\Product;
use Model\Repository\ProductItemRepository;
use Slim\Http\Request;
use Slim\Http\Response;

class AdminProductItemController extends baseController
{

    public $productItemRepository;

    public function __construct($container)
    {
        parent::__construct($container);
        $this->productItemRepository = new ProductItemRepository();
    }

    /**
     * the store method
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function store( Request $request , Response $response , $args )
    {
        $item = $this->productItemRepository->storeItem($args['id'],$request->getParams());
        return $response->withStatus($item['status'])->withJson($item['message']);
    }






}
