<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/1/17
 * Time: 3:50 PM
 */

namespace Model;


use Illuminate\Database\Eloquent\Model;


/**
 * @property string name
 * @property string family
 * @property mixed password
 * @property string email
 * @property int gender
 */
class ProductItem extends Model
{

    protected $table = "product_items";

    protected $fillable = [
        'title','price',
        'description','image',
        'slug','product_id'
    ];



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }


    //todo:: investment cart
    public function investmentCart()
    {
//        return $this->hasMany()
    }



    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attributeList()
    {
        return $this->belongsToMany(AttributeList::class,'product_item_attribute',
            'product_item_id','attribute_list_id')->withPivot('value');
    }
}