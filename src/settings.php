<?php
return [
    'settings' => [
        'name'=>"asdfasfsad",
        // set to false in production
        'displayErrorDetails' => true,
        // Allow the web server to send the content-length header
        'addContentLengthHeader' => false,

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        'determineRouteBeforeAppMiddleware' => false,

        'db' => [
            'driver' => 'mysql',
            'host' =>'127.0.0.1',
            'port' => '3306',
            'database' => 'investory',
            'username' => 'root',
            'password' => '12345678',
            'unix_socket' =>'',
            'charset' => 'utf8',
            'collation' => 'utf8_persian_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => 'InnoDb',
        ],
    ],
];
