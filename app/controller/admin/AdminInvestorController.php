<?php namespace App\controller\admin;


use App\controller\baseController;
use Model\Repository\InvestorBankInformationRepository;
use Model\Repository\InvestorRepository;
use Slim\Http\Request;
use Slim\Http\Response;

class AdminInvestorController extends baseController
{

    public $investorRepository;
    public function __construct($container)
    {
        parent::__construct($container);
        $this->investorRepository = new InvestorRepository();
    }

    /**
     * show all user
     * @param Request $request
     *
     * @param Response $response
     * @return mixed
     */
    public function index(Request $request , Response $response)
    {

        $users = $this->investorRepository->getAllUsers();

        return $response->withStatus(200)->withJson($users);
    }

    /**
     * show user's information
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function show(Request $request , Response $response ,$args)
    {
        $user = $this->investorRepository->showInvestor($args['id']);
        return $response->withStatus($user['status'])->withJson($user);
    }

    /**
     * store bank information for investor
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function storeBankInformation(Request $request , Response $response , $args)
    {
        $bankInformationRepository = new InvestorBankInformationRepository();

        $bank = $bankInformationRepository->storeBank($args['id'],$request->getParams());
       return $response->withStatus($bank['status'])->withJson($bank);
    }

    /**
     * delete bank information's of investor
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function destroyBankInformation(Request $request , Response $response , $args)
    {
        $bankInformationRepository = new InvestorBankInformationRepository();
        $bank = $bankInformationRepository->deleteBank($args['bank_id']);
        return $response->withStatus($bank['status'])->withJson($bank);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function updateBankInformation(Request $request , Response $response , $args)
    {
        $bankInformationRepository = new InvestorBankInformationRepository();
        $bank = $bankInformationRepository->updateBank($args['bank_id'],$request->getParams());
        return $response->withStatus($bank['status'])->withJson($bank);
    }

    /**
     * change password's
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function changePassword(Request $request , Response $response ,$args)
    {
        $password= $request->getParam('password');
        $passwordConfirmation= $request->getParam('password_confirmation');
        $investor = $this->investorRepository->changePasswordByAdmin($args['id'],$password,$passwordConfirmation);
        return $response->withStatus($investor['status'])->withJson($investor);
    }


}
