<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/2/17
 * Time: 11:41 AM
 */

namespace App\controller;

use Model\User;
use Slim\Http\Request;
use Slim\Http\Response;

class HomeController  extends baseController
{


    public function index(Request $request, Response $response)
    {


        $users = User::all();
        return $this->responseJson($users);
    }

}