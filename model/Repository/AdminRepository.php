<?php
namespace Model\Repository;


use Model\Admin;

class AdminRepository
{

    public $_model ;


    public function __construct()
    {
        $this->_model = new Admin();
    }

    /**
     *
     * @param $email
     */
    public function getAdminByEmail($email)
    {
        return $this->_model->where('email',$email)->first();
    }

    /**
     * @param $api_key
     */
    public function getAdminByApiKey($api_key)
    {
        return $this->_model->where('api_key',$api_key)->first();
    }



}