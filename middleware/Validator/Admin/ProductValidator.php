<?php
namespace Middleware\Validator\Admin;


use Valitron\Validator;

class ProductValidator
{

    public $validator;




    public function __invoke($request, $response, $next)
    {
        $validator = new Validator($request->getParams());
        $validator->rule('required', 'email');


        return $this->response($request, $response, $next, $validator);
    }

    /**
     * @param $request
     * @param $response
     * @param $next
     * @param $validator
     * @return mixed
     */
    public function response($request, $response, $next, $validator)
    {
        if ($validator->validate() === false) {
            return $response->withStatus(203)->withJson($validator->errors());
        }


        return $next($request, $response);
    }


}