<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/1/17
 * Time: 3:50 PM
 */

namespace Model;


use Illuminate\Database\Eloquent\Model;


/**
 * @property string name
 * @property string family
 * @property mixed password
 * @property string email
 * @property int gender
 */
class Investor extends Model
{

    protected $table = "investors";


    protected $hidden = ['password','remember_token','api_token'];

    /**
     * get investor's bank informations
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
        public function investorBankInformation()
    {
        return $this->hasMany(InvestorBankInformation::class,'investor_id','id');
    }

    //todo::verification investor




}