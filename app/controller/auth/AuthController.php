<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/2/17
 * Time: 12:05 PM
 */

namespace App\controller\auth;


use App\controller\baseController;
use App\service\Auth\Auth;
use Slim\Http\Request;
use Slim\Http\Response;

class AuthController extends baseController
{


    /**
     * register new user
     * @param Request $request
     * @param Response $response
     * @return string
     */
    public function register(Request $request ,Response $response)
    {

        if( Auth::register($request->getParams()) )
        {
            return $response->withStatus(201)->withJson(['message'=>'registered user .']);
        }
        return $response->withStatus(203)->withJson(['message'=>"incorrect data"]);

    }

    /**
     * login action
     * @param Request $request
     * @param Response $response
     * @return string
     */
    public function login(Request $request,Response $response)
    {
        $username  =$request->getParam('email');
        $password = $request->getParam('password');
        if($key = Auth::login($username,$password))
        {
            return $response->withStatus(202)->withJson(['authorization'=>$key]);
        }

        return $response->withStatus(203)->withJson(['message'=>"username or email incorrect."]);
    }


    




}
