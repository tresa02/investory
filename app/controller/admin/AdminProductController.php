<?php namespace App\controller\admin;


use App\controller\baseController;
use Model\Repository\ProductRepository;
use Slim\Http\Request;
use Slim\Http\Response;

class AdminProductController extends baseController
{

    public $productRepository;
    public function __construct($container)
    {

        parent::__construct($container);
        $this->productRepository = new ProductRepository();

    }

    /**
     * @param Request $request
     *
     * @param Response $response
     * @return mixed
     */
    public function index(Request $request , Response $response)
    {
        $products =$this->productRepository->getAllProduct();
        return $response->withStatus(200)->withJson($products);
    }

    /**
     * show 1 product
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function show(Request $request , Response $response,$args)
    {
        if( $product = $this->productRepository->showProduct( $args['id'] ) )
        {
            return $response->withStatus(200)->withJson($product);
        }
        return $response->withStatus(203)->withJson(['message'=>"the data incorrect"]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    public function store(Request $request , Response $response)
    {
        if( $this->productRepository->saveProduct($request->getParams()) )
        {
            return $response->withStatus(201)->withJson(['message'=>'product has benn created.']);
        }
        return $response->withStatus(203)->withJson(['message'=>'the data incorrect']);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function update(Request $request , Response $response,$args)
    {
        if( $this->productRepository->updateProduct( $args['id'] , $request->getParams() ) )
        {
            return $response->withStatus(202)->withJson(['message'=>"product has been updated."]);
        }
        return $response->withStatus(203)->withJson(['message'=>"the data incorrect"]);
    }


    /**
     * destroy the record product
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function destroy(Request $request , Response $response,$args)
    {
         if($this->productRepository->deleteProduct($args['id']))
         {
             return $response->withStatus(201)->withJson(['message'=>'has been deleted.']);
         }
         return $response->withStatus(203)->withJson(['message'=>"data incorrect"]);
    }

}
