<?php
namespace Model\Repository;


use Model\Product;

class ProductRepository
{

    public $_model ;


    public function __construct()
    {
        $this->_model = new Product();
    }



    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllProduct()
    {
        return $this->_model->with('productItem','category')->get();
    }

    /**
     * save new product in database
     * @param array $data
     * @return bool
     */
    public function saveProduct(array  $data)
    {
        $this->_model->title       = $data['title'];
        $this->_model->description = $data['description'];
        $this->_model->thumbnail   = $data['thumbnail'];
        $this->_model->slug        = $data['slug'];
        $this->_model->category_id = $data['category'];
        $this->_model->stock       = $data['stock'];
        return !!$this->_model->save();
    }

    /**
     * @param $id
     * @param array $data
     * @return bool
     */
    public function updateProduct($id,array $data)
    {
        if($product = $this->_model->find($id)){
            if($product->update($data))
            {
                return true;
            }

        }
        return false;
    }

    /**
     * show an products
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function showProduct($id)
    {
        return $this->_model->with('productItem','category')->where('id',$id)->first();
    }

    public function deleteProduct($id)
    {
        return $this->_model->where('id',$id)->delete();
    }





}