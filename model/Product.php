<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/1/17
 * Time: 3:50 PM
 */

namespace Model;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * @property string title
 * @property string description
 * @property mixed  password
 * @property string thumbnail
 * @property string slug
 * @property string stock
 * @property int category_id
 */
class Product extends Model
{

    use SoftDeletes;

    protected $table = "products";

    protected $fillable=['title','description','category_id','thumbnail','slug','stock','deleted_at'];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productItem()
    {
        return $this->hasMany(ProductItem::class,'product_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }





}