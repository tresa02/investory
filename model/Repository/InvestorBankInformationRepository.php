<?php
namespace Model\Repository;


use Model\Investor;
use Model\InvestorBankInformation;

class InvestorBankInformationRepository
{

    public $_model ;


    public function __construct()
    {
        $this->_model = new InvestorBankInformation();
    }

    /**
     * store new bank information
     * @param $id
     * @param $data
     * @return array
     */
    public function storeBank($id ,$data)
    {
        if($investor = Investor::find($id))
        {

            if($bank = $investor->investorBankInformation()->create($data))
            {

                return ['status'=>201,'bank'=>$bank];
            }
            return ['status'=>203,'message'=>'داده های ارسای نا معتبر است.'];
        }
        return ['status'=>203,'message'=>'داده های ارسای نا معتبر است.'];
    }


    /**
     * delete bank information
     * delete bank
     * @param $id
     * @return array
     */
    public function deleteBank($id)
    {
        //todo ::check with investment profitable
        if($bank = $this->_model->find($id))
        {
            $bank->delete();
            return ['status'=>201,'message'=>'اطلاعات بانکی حذف گردید.'];
        }
        return ['status'=>203,'message'=>'اطلاعات شما کامل نبوده.'];
    }

    /**
     * update bank information
     * update bank
     * @param $id
     * @param array $data
     * @return array
     */
    public function updateBank($id,array  $data)
    {
        if($bank = $this->_model->find($id))
        {
           if($bank->update($data))
           {
               return ['status'=>202, 'message'=>'با موفقیت ویرایش گردید.','bank_information'=>$bank];
           }
        }
        return ['status'=>203,'message'=>'داده های ارسای نا معتبر است.'];
    }

}