<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/2/17
 * Time: 11:38 AM
 */

namespace App\controller;


class baseController
{


    /**
     * get container dependency
     * @var
     */
    public $container;

    /**
     * baseController constructor.
     * @param $container
     */
    public function __construct($container)
    {

        $this->container =$container;
    }

    /**
     * return  json data
     * @param $data
     * @param string $key
     * @return string
     */
    public function responseJson($data,$key= 'items' )
    {
        return json_encode([$key=>$data]);
    }


    /**
     * get property from container
     * @param $name
     * @return null
     */
    public function __get($name)
    {
        if($this->container->{$name})
        {
            return $this->container->{$name};
        }
        return null;
    }
}