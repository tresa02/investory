<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/4/17
 * Time: 9:17 AM
 */

namespace App\service\Auth;


interface AuthInterface
{

    /**
     * user is logged in
     * @param $api_key
     * @return mixed
     * @internal param $api
     */
    public static function check($api_key);


    /**
     * login user
     * @param $username
     * @param $password
     * @return mixed
     */
    public static function login($username ,$password);


    /**
     * logout user
     * @param $api_key
     * @return mixed
     */
    public static function logout($api_key);


    /**
     * get user information
     * @param $api_key
     * @return mixed
     */
    public static function user($api_key);

    /**
     * register new user
     * @param array $data
     * @return mixed
     */
    public static function register(array  $data);


}