<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/7/17
 * Time: 12:19 PM
 */

namespace Tests\Functional;


use Model\Investor;

class AdminInvestorTest extends BaseTestCase
{

    public $addressInvestor = '/admin/investor';


    /**
     * test show all investors '/admin/investors'
     */
    public function testAdmin_show_all_investors()
    {

        $investors = Investor::all();
        $request  =  $this->get($this->addressInvestor);

        $this->assertEquals(200,$request->getStatusCode());
        foreach ($investors as $investor)
        {
            $this->assertContains($investor->email,(string) $request->getBody());
        }
    }


    /**
     * test show investor info '/admin/investor/{id)'
     */
    public function testAdmin_show_investor_info()
    {
        $investor = Investor::inRandomOrder()->first();
        if($investor)
        {
            $request = $this->get($this->addressInvestor.'/'.$investor->id );
            $this->assertEquals(200,$request->getStatusCode());
            $this->assertContains($investor->email , (string) $request->getBody());
        }
    }


    /**
     * test didn't show investor if bad id given
     */
    public function testAdmin_did_not_show_investor_bad_id_given()
    {
        $request = $this->get($this->addressInvestor.'/3544684353413131321321' );
        $this->assertEquals(203,$request->getStatusCode());
    }


    /**
     * test store new bank information for investor
     */
    public function testAdmin_store_bank_information()
    {
        $investor = Investor::inRandomOrder()->first();
        if ($investor) {
            $data = [
                'bank_name' => 'test.name',
                'bank_cart_number' => 'test.number',
                'bank_account_number' => 'test.number'
            ];
            $request = $this->post($this->addressInvestor . '/' . $investor->id . '/add-bank', $data);
            $this->assertEquals(201, $request->getStatusCode());

        }

    }


    /**
     * test didn't store when bad id given or bad data given
     *
     */
    public function testAdmin_did_not_store_bank_information_bad_data_given()
    {

        $request = $this->post($this->addressInvestor . '/54465474878976435/add-bank', []);
        $this->assertEquals(203, $request->getStatusCode());
    }


    /**
     * test delete bank information from
     */
    public function testAdmin_delete_bank_information_investor()
    {
        $investor = Investor::whereHas('investorBankInformation')->with(['investorBankInformation'=>function($query){
            $query->first();
        }])->first();

        if($investor){
            $address = $this->addressInvestor.'/'.$investor->id.'/bank/'.$investor->investorBankInformation[0]->id;
            $request = $this->delete($address);
            $this->assertEquals(201,$request->getStatusCode());
        }
    }



    /**
     * test did not delete bank information from
     */
    public function testAdmin_did_not_delete_bank_information_investor()
    {
        $address = $this->addressInvestor.'/sdfsdfsdfs/bank/sdfsdf';
        $request = $this->delete($address);
        $this->assertEquals(203,$request->getStatusCode());
    }


    /**
     * test update bank information for investor
     */
    public function testAdmin_update_bank_information()
    {
        $investor = Investor::whereHas('investorBankInformation')->with(['investorBankInformation'=>function($query){
            $query->first();
        }])->first();
        if ($investor) {
            $data = [
                'bank_name' => 'test.name',
                'bank_cart_number' => 'test.number',
                'bank_account_number' => 'test.number'
            ];
            $address = $this->addressInvestor.'/'.$investor->id.'/bank/'.$investor->investorBankInformation[0]->id.'/update';
            $request = $this->put($address, $data);
            $this->assertEquals(202, $request->getStatusCode());
        }

    }


    /**
     * test didn't update bank information for investor
     */
    public function testAdmin_did_not_update_bank_information()
    {
        $data = [
            'bank_name' => 'test.name',
            'bank_cart_number' => 'test.number',
            'bank_account_number' => 'test.number'
        ];
        $address = $this->addressInvestor.'/cgvbcxvbxc/bank/sfgsdg/update';
        $request = $this->put($address, $data);
        $this->assertEquals(203, $request->getStatusCode());
    }


    /**
     * test change password investor /investor/{id}/bank/{bank_id}/change-password
     */
    public function testAdmin_change_password_Investor()
    {
        $investor = Investor::inRandomOrder()->first();
        if($investor) {

            $data = [
                'newPassword' => '123456',
                'passwordConfirmation'=>'123456'
            ];
            $request = $this->put($this->addressInvestor . "/{$investor->id}/change-password", $data);
            $this->assertEquals(202,$request->getStatusCode());
        }
    }


    /**
     * test didn't change password investor '/investor/{id}/change-password' bad id given
     */
    public function testAdmin_did_not_change_password_Investor_if_bad_id_given()
    {
        $investor = Investor::inRandomOrder()->first();
        if($investor)
        {
            $data = [
                'password' => '123456',
                'password_confirmation'=>'1234f56'
            ];
            $request = $this->put($this->addressInvestor . "/sad/change-password", $data);
            $this->assertEquals(203,$request->getStatusCode());
        }
    }

}
