<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/4/17
 * Time: 9:17 AM
 */

namespace App\service\Auth;


use Model\Admin;

class AdminAuth implements AuthInterface
{

    public static  $typo ='ghsdR4';


    /***
     * check user is logged in
     * @param $api_key encoded key
     * @return bool
     */
    public static function  check($api_key)
    {

        return !!Admin::where('api_key',$api_key)->first();
    }


    /**
     * login user
     * @param $username
     * @param $password
     * @return mixed
     */
    public static function login($username, $password)
    {
        $password =self::encrypt($password);

        $user = Admin::where("email",$username)->where('password',$password)->first();
        if( $user )
        {
            $key = self::encrypt($user->email);
            $user->api_key = $key;
            $user->save();
            return $user->api_key;
        }
        return false;
    }

    /**
     * logout user
     * @param $api_key
     * @return bool
     */
    public static function logout($api_key)
    {
        return !! Admin::where('api_key',$api_key)->update(['api_key'=>rand(0,99999999)]);
    }

    /**
     * get user
     * @param $api_key
     * @return mixed
     */
    public static function user($api_key)
    {
        return Admin::where('api_key',$api_key)->first();
    }

    /**
     * hash string
     * @param $string
     * @return mixed
     */
    private static function encrypt($string)
    {
        return hash('sha256',$string);
    }

    /**
     * register new  user
     * @param array $data
     * @return mixed
     */
    public static function register(array $data)
    {
        return Admin::create($data);
    }



}