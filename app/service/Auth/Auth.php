<?php
namespace App\service\Auth;


use Model\Investor;
use Model\Repository\InvestorRepository;

class Auth implements AuthInterface
{
    //todo :: set query to repository
    public static  $typo ='axDv3h';


    /***
     * check Investor is logged in
     * @param $api_key encoded key
     * @return bool
     */
    public static function  check($api_key)
    {
        $repository = new InvestorRepository();
        return !!$repository->getUserByApiKey($api_key);
    }


    /**
     * login Investor
     * @param $username
     * @param $password
     * @return mixed
     */
    public static function login($username, $password)
    {

        $password =self::encrypt($password);
        $Investor = Investor::where("email",$username)->where('password',$password)->first();
        if( $Investor )
        {
            $key = self::encrypt($Investor->email);
            $Investor->api_key = $key;
            $Investor->save();
            return $Investor->api_key;
        }
        return false;

    }

    /**
     * logout Investor
     * @param $api_key
     * @return bool
     */
    public static function logout($api_key)
    {
        if(Investor::where('api_key',$api_key)->update(['api_key'=>'']))
        {
            return true;
        }
        return false;
    }

    /**
     * get Investor
     * @param $api_key
     * @return mixed
     */
    public static function user($api_key)
    {
        return (new InvestorRepository())->getUserByApiKey($api_key);
    }

    /**
     * hash string
     * @param $string
     * @return mixed
     */
    private static function encrypt($string)
    {
        return hash('sha256',$string);
    }

    /**
     * register new  Investor
     * data most validate before call this method
     * @param array $data
     * @return mixed
     */
    public static function register(array $data)
    {
        return Investor::create($data);
    }



}