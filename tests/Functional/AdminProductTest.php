<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/7/17
 * Time: 10:12 AM
 */

namespace Tests\Functional;


use Model\Category;
use Model\Product;
use Model\Repository\ProductRepository;
use phpDocumentor\Reflection\Types\This;

class AdminProductTest extends BaseTestCase
{

    public $addressProducts = '/admin/products';

    /**
     * test all product page in route 'admin/products'
     */
    public function testAdmin_Product_Index()
    {
        $products = (new ProductRepository())->getAllProduct();

        $request  = $this->get($this->addressProducts);

        $this->assertEquals(200,$request->getStatusCode());
        $body = $request->getBody();

        foreach ($products as $product)
        {
            $this->assertContains($product->title,(string) $body);
        }

    }

    /**
     * test show product page
     */
    public function testAdmin_Show_Product_If_Exist()
    {
        $randomProduct = Product::inRandomOrder()->first();

        $request = $this->get($this->addressProducts.'/'.$randomProduct->id);
        $this->assertEquals(200,$request->getStatusCode());
        $this->assertContains($randomProduct->title , (string ) $request->getBody());
        
    }
    /**
     * test  didn't show product if id not exists
     */
    public function testAdmin_did_not_show_product_if_bad_id()
    {
        $request  = $request = $this->get($this->addressProducts.'/dsg35sdfg4sdf34g3');
        $this->assertEquals(203,$request->getStatusCode());
    }

    /**
     * test store product if correct data send to it
     */
    public function testAdmin_store_product_correct_value()
    {
        $data =[
            'title'=>str_random(),
            'description'=>str_random(),
            'thumbnail'=>'test.png',
            'slug'=>str_random(),
            'category'=>Category::inRandomOrder()->first()->id,
            'stock'=>rand(0,100)
        ];
        $request = $this->post($this->addressProducts.'/store',$data);
        $this->assertEquals(201,$request->getStatusCode());
    }

    /**
     * test update product with correct value
     */
    public function testAdmin_update_product_with_correct_data()
    {
        $product = Product::inRandomOrder()->first();
        $data =[
            'title'=>str_random(),
            'description'=>str_random(),
            'thumbnail'=>'test.png',
            'slug'=>str_random(),
            'category_id'=>Category::inRandomOrder()->first()->id,
            'stock'=>rand(0,100)
        ];
        if($product) {
            $request = $this->put($this->addressProducts . "/{$product->id}" . '/update', $data);
            $this->assertEquals(202, $request->getStatusCode());
        }
    }

    /**
     * test update didn't save id is not defined in database
     */
    public function testAdmin_update_product_with_incorrect_id()
    {
        $data =[
            'title'       => str_random(),
            'description' => str_random(),
            'thumbnail'   => 'test.png',
            'slug'        => str_random(),
            'category_id' => Category::inRandomOrder()->first()->id,
            'stock'       => rand(0,100)
        ];
        $request = $this->put($this->addressProducts.'/{id}/update',[]);

        $this->assertEquals(203,$request->getStatusCode());
    }

    /**
     * test delete product
     */
    public function testAdmin_delete_product()
    {
        $product = Product::inRandomOrder()->first();
        if($product)
        {
            $request = $this->delete($this->addressProducts.'/'.$product->id);
            $this->assertEquals(201,$request->getStatusCode());
        }
    }

    /**
     * test didn't delete product if bad id request
     */
    public function testAdmin_did_not_delete_if_bad_id()
    {
        $request = $this->delete($this->addressProducts.'/sdfsdf sdf sd fsd fsdfs');
        $this->assertEquals(203,$request->getStatusCode());
    }




    /**
     *
     */






}