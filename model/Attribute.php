<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/1/17
 * Time: 3:50 PM
 */

namespace Model;


use Illuminate\Database\Eloquent\Model;


/**
 * @property string name
 * @property string family
 * @property mixed password
 * @property string email
 * @property int gender
 */
class Attribute extends Model
{

    protected $table = "attributes";



}