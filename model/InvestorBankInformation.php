<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/1/17
 * Time: 3:50 PM
 */

namespace Model;


use Illuminate\Database\Eloquent\Model;


/**
 * @property string name
 * @property string family
 * @property mixed password
 * @property string email
 * @property int gender
 */
class InvestorBankInformation extends Model
{

    protected $table = "investor_bank_informations";


    protected $fillable = ['investor_id','bank_name','bank_cart_number','bank_account_number'];

    /**
     * get investor
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investor()
    {
        return $this->belongsTo(Investor::class,'investor_id','id');
    }





}