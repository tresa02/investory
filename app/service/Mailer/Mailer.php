<?php
/**
 * Created by PhpStorm.
 * User: tresa
 * Date: 11/4/17
 * Time: 11:51 AM
 */

namespace App\service\Mailer;

use PHPMailer\PHPMailer\Exception;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
Use PHPMailer\PHPMailer\PHPMailer;
class Mailer
{

    public $mail;

    /**
     * set config
     * Mailer constructor.
     */
    public function __construct()
    {
        $config =include __DIR__.'/../../../src/mail.php';//todo ::set it with $app
        $this->mail = new PHPMailer($config['exception']);

        $this->mail->SMTPDebug = $config['debug'];

        if($config['is_smtp'])
        {
            // Enable verbose debug output
            $this->mail->isSMTP();
        }
        // Set mailer to use SMTP
        $this->mail->Host       = $config['host'];                  // Specify main and backup SMTP servers
        $this->mail->SMTPAuth   = $config['smtp_Auth'];             // Enable SMTP authentication
        $this->mail->Username   = $config['username'];              // SMTP username
        $this->mail->Password   = $config['password'];              // SMTP password
        $this->mail->SMTPSecure = $config['secure_type'];           // Enable TLS encryption, `ssl` also accepted
        $this->mail->Port       = $config['port'];                  // TCP port to connect to
        $this->mail->setFrom(  $config['from'], $config['name']);
    }

    /**
     * send an email to user's
     * @param $to
     * @param $subject
     * @param $body
     * @param $altbody
     */
    public function send($to ,$subject , $body ,$altbody)
    {
        try{
            $this->mail->addAddress($to);


            //Content
            $this->mail->isHTML(true);
            $this->mail->Subject = $subject;
            $this->mail->Body    = $body;
            $this->mail->AltBody = ($altbody)?:$body;

            $this->mail->send();
        } catch (Exception $e)
        {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $this->mail->ErrorInfo;
        }
    }







}