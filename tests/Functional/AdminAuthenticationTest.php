<?php namespace Tests\Functional;

use Model\Admin;

class AdminAuthenticationTest extends BaseTestCase
{

    public $loginAddress='/admin/login';
    public $logoutAddress='/admin/logout';

    /**
     * test login admin in system with no parameter
     */
    public function testPostAdminLoginWithoutParameters()
    {
        $request = $this->post($this->loginAddress,[]);
        $this->assertEquals(203,$request->getStatusCode());
    }

    /**
     * test login with incorrect parameter
     */
    public function testPostAdminLoginIncorrectParameter()
    {
        $request = $this->post($this->loginAddress,['email'=>'a@a.com','password'=>'12345r gdfg d6']);
        $this->assertEquals(203,$request->getStatusCode());
    }

    /**
     * test login with correct parameter
     */
    public function testPostAdminLogin()
    {
        $request = $this->post($this->loginAddress ,['email'=>'a@a.com','password'=>'123456'] );


        $this->assertContains('authorization',(string )$request->getBody());
    }

    /**
     * test logout admin
     */
    public function testAdminLogoutThatLoggedIn()
    {
        $admin = $this->beAdmin();

        $request = $this->post($this->logoutAddress,['Authorization'=>$admin->api_key])->withHeader('Authorization',$admin->api_key);
        $this->assertEquals(200,$request->getStatusCode());
    }

    /**
     * test try to logged out admin that not logging in
     */
    public function testAdminLogoutThatNotLoggedIn()
    {
        $request = $this->post($this->logoutAddress)->withHeader('Authorization',"asdsadek hfsudhfshdkfjhsdkj fhjskdhjkhsdjkfhsdh");
        $this->assertEquals(203,$request->getStatusCode());
    }


}
